#!/bin/bash

export TAG_NAME=16.13.1
export IMAGE_NAME=petrynchynbinary/alpine-nginx-node

docker build -t $IMAGE_NAME:$TAG_NAME . --no-cache

echo $DOCKER_PWD | docker login -u $DOCKER_LOGIN --password-stdin

docker push $IMAGE_NAME:$TAG_NAME

# docker run --rm -it -p 8080:80 petrynchynbinary/nginx-node-alpine:0.1.0 sh
